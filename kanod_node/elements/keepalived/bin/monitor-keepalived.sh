#!/bin/bash
#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


while :; do
    curl -sk https://127.0.0.1:6443/healthz 1>&2 > /dev/null
    isOk=$?
    isActive=$(systemctl show -p ActiveState keepalived.service | cut -d'=' -f2)
    if [ "$isOk" == "0" ] &&  [ "$isActive" != "active" ]; then
        logger 'API server is healthy, however keepalived is not running, starting keepalived'
        echo 'API server is healthy, however keepalived is not running, starting keepalived'
        sudo systemctl start keepalived.service
    elif [ "$isOk" != "0" ] &&  [ "$isActive" == "active" ]; then
        logger 'API server is not healthy, however keepalived running, stopping keepalived'
        echo 'API server is not healthy, however keepalived running, stopping keepalived'
        sudo systemctl stop keepalived.service
    fi
    sleep 5
done
