#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import base64
import bcrypt
import os
from pathlib import Path
import subprocess
import tempfile
import sys
import yaml

from kanod_configure import common


def b64(s: str):
    '''encode a utf-8 string in base64'''
    return base64.b64encode(s.encode('utf-8')).decode('ascii')


def define_var(environ, config, field, var):
    '''Transfer a var from the yaml config to a dict (env variables)

    :param environ: dictionary of environment variables
    :param config: yaml configuration
    :param field: field in the yaml configuration
    :param var: variable to define in the environment
    '''
    val = config.get(field, None)
    if val is not None:
        environ[var] = val
    else:
        print(f'Warning stack: variable {var} is empty.')
        environ[var] = ''


def htpasswd(username, passwd):
    salt = bcrypt.gensalt(rounds=5)
    crypted = bcrypt.hashpw(passwd.encode('utf-8'), salt).decode('ascii')
    return f"{username}:{crypted}"


def setup_base(environ, config):
    '''Setup base variables

    Mainly proxies, path, kubeconfig and Nexus.

    :param environ: dictionnary of env variables being defined
    :param config: source configuration (was a yaml file)
    '''
    proxy = config.get('proxy', {})
    define_var(environ, proxy, 'http', 'MGT_HTTP_PROXY')
    define_var(environ, proxy, 'https', 'MGT_HTTPS_PROXY')
    define_var(environ, proxy, 'no_proxy', 'MGT_NO_PROXY')
    nexus = config.get('nexus', {})
    define_var(environ, nexus, 'maven', 'NEXUS')
    define_var(environ, nexus, 'docker', 'NEXUS_REGISTRY')
    environ['KUBECONFIG'] = '/etc/kubernetes/admin.conf'
    environ['PATH'] = (
        '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin')
    certificate = nexus.get('certificate', ' ')
    environ['BASE64_REPO_CA'] = b64(certificate)


def setup_ironic(environ, config):
    '''Setup ironic variables

    :param environ: dictionnary of env variables being defined
    :param config: source configuration (was a yaml file)
    '''
    ironic = config.get('ironic', None)
    if ironic is None:
        return
    define_var(environ, ironic, 'interface', 'PXE_ITF')
    define_var(environ, ironic, 'ip', 'IRONIC_IP')
    define_var(environ, ironic, 'ipa_kernel_url', 'IPA_KERNEL_URL')
    define_var(environ, ironic, 'ipa_ramdisk_url', 'IPA_RAMDISK_URL')
    dhcp = ironic.get('dhcp', {})
    with tempfile.NamedTemporaryFile(
            mode='w', encoding='utf-8', delete=False) as fd:
        environ['DHCP_FILE'] = fd.name
        yaml.dump(dhcp, fd)


def setup_git_variables(environ, config):
    '''Setup git information

    :param environ: dictionnary of env variables being defined
    :param config: source configuration (was a yaml file)
    '''
    gits = config.get('git', {})
    for (section_name, varname) in [
        ('baremetal', 'BAREMETAL'), ('projects', 'PROJECTS')
    ]:
        section = gits.get(section_name, {})
        url = section.get('url', None)
        if url is None:
            continue
        environ[f'{varname}_GIT'] = url
        username = section.get('username', '')
        if username is not None:
            environ[f'BASE64_{varname}_USERNAME'] = b64(username)
        password = section.get('password', '')
        if password is not None:
            environ[f'BASE64_{varname}_PASSWORD'] = b64(password)


def setup_proxy(environ, stack):
    tpm = stack.get('tpm', None)
    use_tpm = tpm is not None
    proxy = stack.get('proxy', None)
    key = proxy.get('key', None)
    cert = proxy.get('certificate', None)
    ip_address = proxy.get('ip', None)
    environ['IP_ADDRESS'] = ip_address
    if key is not None and cert is not None and ip_address is not None:
        folder = Path('/etc/nginx/certs')
        os.mkdir(folder)
        with open(folder.joinpath('key.pem'), 'w') as fd:
            fd.write(key)
            fd.write('\n')
        with open(folder.joinpath("cert.pem"), 'w') as fd:
            fd.write(cert)
            fd.write('\n')
        common.render_template(
            'stack_proxy.tmpl',
            'etc/nginx/sites-available/proxy',
            {'ip_address': ip_address, 'use_tpm': use_tpm})


def setup_argocd(environ, config):
    vault = config.get('vault', {})
    environ['USE_VAULT'] = '1' if vault != {} else '0'
    environ['VAULT_URL'] = vault.get('url', 'x')
    environ['VAULT_CA'] = vault.get('ca', ' ')
    environ['VAULT_CA_B64'] = b64(vault.get('ca', ' '))
    environ['VAULT_K8S'] = vault.get('role', 'lcm')


def setup_tpm(environ, stack):
    tpm = stack.get('tpm', {})
    proxy_cert = stack.get('proxy', {}).get('certificate', None)
    use_tpm = tpm != {}
    environ['TPM'] = '1' if use_tpm else '0'
    environ['REGISTRAR_VERSION'] = tpm.get('version', 'v0.0.0')
    environ['REGISTRAR_CA_B64'] = (
        b64(proxy_cert) if proxy_cert is not None else '')
    environ['REGISTRAR_PORT'] = '8443'
    environ['TPM_AUTH_URL'] = tpm.get('auth_url', '')
    environ['TPM_AUTH_CA_B64'] = b64(tpm.get('auth_ca', ''))


def launch_stack(arg: common.RunnableParams):
    '''Launch the deployment of kanod stack

    :param config: source configuration (was a yaml file)
    '''
    config = arg.conf
    stack = config.get('stack', None)
    environ = {'HOME': '/root'}
    if stack is None:
        print(' * not configured')
        return
    define_var(environ, stack, 'version', 'STACK_VERSION')
    setup_base(environ, config)
    setup_proxy(environ, stack)
    setup_ironic(environ, stack)
    setup_git_variables(environ, stack)
    setup_argocd(environ, config)
    setup_tpm(environ, stack)
    certs = dict(stack.get('git', {}))
    nexus_cert = config.get('nexus', {}).get('certificate', None)
    if nexus_cert is not None:
        certs['nexus'] = nexus_cert
    common.setup_certificates(
        certs,
        '/etc/kanod-configure/git-certs')
    with open(
        '/etc/kanod-configure/stack.env',
        'w', encoding='utf-8'
    ) as fd:
        for (key, value) in environ.items():
            fd.write(f'{key}="{value}"\n')
    process = subprocess.run(
        ['/usr/local/bin/kanod-stack'], env=environ,
        stdout=sys.stdout, stderr=subprocess.STDOUT)
    # print(process.stdout.decode('utf-8'))
    if process.returncode == 0:
        print('Stack deployed')
    else:
        raise Exception('Stack deployment failed')


def hook_stack_vault(arg: common.RunnableParams):
    conf = arg.conf
    k8s_cfg = conf.setdefault('kubernetes', {})
    k8s_cfg['vault_name'] = 'lcm'


common.register('--> Stack hook for vault registration', 399, hook_stack_vault)
common.register('Launching kanod stack', 500, launch_stack)
