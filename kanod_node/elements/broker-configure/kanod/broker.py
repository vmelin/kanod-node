#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import os
from pathlib import Path
import subprocess
import sys

from kanod_configure import common


def define_var(environ, config, field, var):
    '''Transfer a var from the yaml config to a dict (env variables)

    :param environ: dictionary of environment variables
    :param config: yaml configuration
    :param field: field in the yaml configuration
    :param var: variable to define in the environment
    '''
    val = config.get(field, None)
    if val is not None:
        environ[var] = val
    else:
        print(f'Warning broker: variable {var} is empty.')
        environ[var] = ''


def setup_base(environ, config):
    '''Setup base variables

    Mainly proxies, path, kubeconfig and Nexus.

    :param environ: dictionnary of env variables being defined
    :param config: source configuration (was a yaml file)
    '''
    print('Configure environment')
    proxy = config.get('proxy', {})
    define_var(environ, proxy, 'http', 'MGT_HTTP_PROXY')
    define_var(environ, proxy, 'https', 'MGT_HTTPS_PROXY')
    define_var(environ, proxy, 'no_proxy', 'MGT_NO_PROXY')
    nexus = config.get('nexus', {})
    define_var(environ, nexus, 'maven', 'NEXUS')
    define_var(environ, nexus, 'docker', 'NEXUS_REGISTRY')
    environ['KUBECONFIG'] = '/etc/kubernetes/admin.conf'
    environ['PATH'] = (
        '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin')


def setup_proxy(environ, broker):
    print('Configure nginx proxy')
    proxy = broker.get('proxy', None)
    key = proxy.get('key', None)
    cert = proxy.get('certificate', None)
    ip_address = proxy.get('ip', None)
    environ['IP_ADDRESS'] = ip_address
    if key is not None and cert is not None and ip_address is not None:
        folder = Path('/etc/nginx/certs')
        os.mkdir(folder)
        with open(folder.joinpath('key.pem'), 'w') as fd:
            fd.write(key)
            fd.write('\n')
        with open(folder.joinpath("cert.pem"), 'w') as fd:
            fd.write(cert)
            fd.write('\n')
        common.render_template(
            'proxy-broker.tmpl',
            'etc/nginx/sites-available/proxy',
            {'ip_address': ip_address})


def launch_broker(arg: common.RunnableParams):
    '''Launch the deployment of the redfish broker

    :param config: source configuration (was a yaml file)
    '''
    config = arg.conf
    broker = config.get('broker', None)
    environ = {'HOME': '/root'}
    if broker is None:
        print('* No broker configuration')
        return
    define_var(environ, broker, 'version', 'BROKER_VERSION')
    setup_base(environ, config)
    setup_proxy(environ, broker)
    process = subprocess.run(
        ['/usr/local/bin/launch-broker'], env=environ,
        stdout=sys.stdout, stderr=subprocess.STDOUT)
    if process.returncode == 0:
        print('* Broker deployed')
    else:
        raise Exception('Broker deployment failed')


common.register('Redfish Broker', 500, launch_broker)
