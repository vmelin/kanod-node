#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from cloudinit import subp


def get_k8s_pause_container():
    pause_container = ""
    ret = subp.subp(['kubeadm', 'config', 'images', 'list'])
    containers = ret[0]
    for container in containers.split('\n'):
        if "k8s.gcr.io/pause:" in container:
            pause_container = container
    return pause_container


def local_pause_container(conf):
    '''Gives back the full name to the pause container

    This is the one used by current kubernetes version. It is used
    to be injected in CRI configuration.'''
    nexus = conf.get('nexus', {})
    registry = nexus.get('docker', None)
    pause_container = get_k8s_pause_container()

    return f'{registry}/{pause_container}'
