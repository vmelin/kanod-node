#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from cloudinit import subp

from . import kubernetes_aux
from . import kanod_containers
from kanod_configure import common


def get_containerd_auth(conf):
    '''get authentication tokens for private registries'''
    containers_vars = conf.get('containers', {})
    raw_auths = containers_vars.get('auths', None)
    if raw_auths is None:
        return None
    auths = [
        {
            "repository": cell.get('repository', ''),
            "username": cell.get('username', ''),
            "password": cell.get('password', ''),
        }
        for cell in raw_auths
    ]
    return auths


def container_engine_containerd_config(conf):
    '''Configure the containerd container engine'''
    proxy_vars = conf.get('proxy')
    if proxy_vars is not None:
        common.render_template(
            'container_engine_proxy.tmpl',
            'etc/systemd/system/containerd.service.d/http-proxy.conf',
            proxy_vars
        )
    insecure_registries = kanod_containers.get_insecure_registries(conf)
    mirrors = conf.get('containers', {}).get('registry_mirrors', None)
    auths = get_containerd_auth(conf)
    common.render_template(
        'containerd_custom_config.tmpl',
        'etc/containerd/config.d/custom-config.toml',
        {'local_pause_container': kubernetes_aux.local_pause_container(conf),
         'insecures': insecure_registries,
         'mirrors': mirrors, 'auths': auths})
    common.render_template(
        'containerd_kubelet.tmpl',
        'etc/default/kubelet',
        {})
    common.render_template(
        'crictl.tmpl',
        'etc/crictl.yaml',
        {'socket': '/var/run/containerd/containerd.sock'})
    subp.subp(['systemctl', 'daemon-reload'])
    subp.subp(['systemctl', 'enable', 'containerd'])
    subp.subp(['systemctl', 'start', 'containerd'])


def register_containerd_engine(arg: common.RunnableParams):
    engines = arg.system.setdefault('container-engines', {})
    engines['containerd'] = container_engine_containerd_config


common.register('Register containerd engine', 50, register_containerd_engine)
