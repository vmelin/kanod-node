#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


set -eu

echo "CRI-O installation"
if [ "$DISTRO_NAME" == "ubuntu" ]; then
    OS="xUbuntu_$(lsb_release -r -s)"
    apt-get install -y curl

    curl -L "https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$DIB_CRIO_VERSION/$OS/Release.key" | gpg --dearmor > "/usr/share/keyrings/crio-$DIB_CRIO_VERSION-keyring.gpg"
    curl -L "https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/Release.key" | gpg --dearmor > /usr/share/keyrings/libcontainers-keyring.gpg

    mkdir -p /etc/apt/sources.list.d
    cat > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list <<EOF
deb [arch=amd64 signed-by=/usr/share/keyrings/libcontainers-keyring.gpg] https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/${OS}/ /
EOF
    cat > "/etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:${DIB_CRIO_VERSION}.list" <<EOF 
deb [arch=amd64 signed-by=/usr/share/keyrings/crio-${DIB_CRIO_VERSION}-keyring.gpg] http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/${DIB_CRIO_VERSION}/${OS}/ /
EOF
elif [ "$DISTRO_NAME" == "centos" ]; then
    if [ "$DIB_RELEASE" == '8-stream' ]; then
        OS="CentOS_8_Stream"
    else
        OS="CentOS_${DIB_RELEASE}"
    fi
    curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable.repo "https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/devel:kubic:libcontainers:stable.repo"
    curl -L -o "/etc/yum.repos.d/devel:kubic:libcontainers:stable:cri-o:$DIB_CRIO_VERSION.repo" "https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$DIB_CRIO_VERSION/$OS/devel:kubic:libcontainers:stable:cri-o:$DIB_CRIO_VERSION.repo"

else
    echo 'Unsupported distribution'
    exit 1
fi

echo "CRI-O Configured for install"
