#  Copyright (C) 2020-2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from cloudinit import subp
from kanod_configure import common

from . import kanod_containers
from . import kubernetes_aux


def strip_scheme(e):
    if e.startswith('http://'):
        return e[7:]
    elif e.startswith('https://'):
        return e[8:]
    else:
        return e


def container_engine_crio_config(conf):
    '''Configure the crio container engine'''
    proxy_vars = conf.get('proxy')
    if proxy_vars is not None:
        common.render_template(
            'container_engine_proxy.tmpl',
            'etc/systemd/system/crio.service.d/http-proxy.conf',
            proxy_vars
        )
    insecure_registries = kanod_containers.get_insecure_registries(conf)
    mirrors = [
        strip_scheme(e)
        for e in conf.get('containers', {}).get('registry_mirrors', [])
    ]
    common.render_template(
        'crio_registries.tmpl',
        'etc/containers/registries.conf.d/50-kanod.conf',
        {'insecures': insecure_registries,
         'mirrors': mirrors})
    common.render_template(
        'crio_kubelet.tmpl',
        'etc/default/kubelet',
        {})
    common.render_template(
        'crictl.tmpl',
        'etc/crictl.yaml',
        {'socket': '/var/run/crio/crio.sock'})

    pause_container = kubernetes_aux.local_pause_container(conf)
    command = [
        'sed', '-i',
        f's!"k8s.gcr.io/pause:.*"!"{pause_container}"!',
        "/etc/crio/crio.conf"]
    subp.subp(command)

    subp.subp(['systemctl', 'daemon-reload'])
    subp.subp(['systemctl', 'enable', 'crio'])
    subp.subp(['systemctl', 'start', 'crio'])


def register_crio_engine(arg: common.RunnableParams):
    engines = arg.system.setdefault('container-engines', {})
    engines['crio'] = container_engine_crio_config


common.register('Register crio engine', 50, register_crio_engine)
