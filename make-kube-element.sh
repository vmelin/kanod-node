#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


RELEASE=${1:-v1.18.8}

function sync_images {
    echo "Retrieving kubeadm"
    echo
    TEMP=$(mktemp -d)
    curl -L "https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/kubeadm" -o "${TEMP}/kubeadm" 
    chmod +x "${TEMP}/kubeadm"
    echo "Image sync"
    echo
    docker login --username "${NEXUS_KANOD_USER}" --password "${NEXUS_KANOD_PASSWORD}" "${NEXUS_REGISTRY}"
    for img in $("${TEMP}/kubeadm" config \
                images list --kubernetes-version "${RELEASE}"); do
        echo "- Syncing ${img}"
        if curl --output /dev/null --silent --head --fail "${NEXUS_REGISTRY}/v2/${img/:/\/manifests/}"; then
            echo "  ${img} already uploaded"
        else
            echo "  pushing ${img} to local registry"
            docker pull "${img}"
            docker tag "${img}" "${NEXUS_REGISTRY}/${img}"
            docker push "${NEXUS_REGISTRY}/${img}"
            if [[ ${img} == *"coredns/coredns"* ]]; then
                short="${NEXUS_REGISTRY}/${img/coredns\/coredns/coredns}"
                docker tag "${img}" "${short}"
                docker push "${short}"
            fi
            if [ "${KANOD_PRUNE_IMAGES:-0}" == "1" ]; then
                docker image rm "${img}"
                docker image rm "${NEXUS_REGISTRY}/${img}"
            fi
        fi
    done
    echo "Clean-up"
    rm -rf "${TEMP}"
}

sync_images
