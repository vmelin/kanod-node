# Kanod Image Builder

## Installation

You need ```disk-image-create``` in your path. It is supplied by the
Openstack ```diskimage-builder``` project. You must use a recent version
(>= 3.2.1)  to create images that are both EFI and
BIOS bootable. Use ```pip3 install diskimage-builder```.

The tool requires various packages to run mainly related to package management
both for Ubuntu and CentOS. The following packages are necessary on an Ubuntu
system:

* `qemu-utils`
* `debootstrap`
* `gdisk`
* `kpartx`
* `dosfstools`
* `rpm`
* `yum-utils`

on a CentOS system, you must install:

* `qemu-img`
* `debootstrap`
* `gdisk`
* `yum-utils`
* `policycoreutils-python-utils`

and *disable* SELinux.

## Usage

You must first create "elements" for Kubernetes:
```
./make-kube-element <version>
```
where ```<version>``` is your Kubernetes version (default `v1.18.4`)

* To build an Ubuntu 18 image with a given Kubernetes version:
  ```
  ./make-image.sh -u [-k <version>]
  ```
* To build a Centos 8 image with a given Kubernetes version:
  ```
  ./make-image.sh -c [-k <version>]
  ```

## make-image.sh

Creates a qcow2 OS image incorporating Kubernetes and a set of utilities such as
keepalived. Tested for bionic and CentOS 8. It should work for Focal but probably not
for CentOS 7.

* `-u` builds an Ubuntu image (default)
* `-c` builds a CentOS image
* `-v <distrib>` OS version (default to bionic or 8 for CentOS)
* `-d` creates a debug image with a default password for user `devuser` of `secret` 
   accessible from the console if the network is broken. It must **never be used in production**.
* `-k <version>` Kubernetes version (format `vX.Y.Z`)
* `-o <filename.qcow2>` name of the image generated
* `-p <package list>` a comma separated list of additional packages (debian or rpm) for a simple customization of images
* `-s <size>` size of generated disk (usually extended unless partitioning)
* `-e` extended mode with added partition for Ceph

## make-kube-element.sh

Prepare the diskimage builder *element* that adds kubernetes binaries to an image. The
only argument is the Kubernetes version in `vX.Y.Z` format.

## burner-build.sh - Creation of an image burner

A burner is a small Linux distribution (kernel and ramfs) that has the capability to mount a
CDROM and write the image it contains on an identified disk. The CDROM should contain at the
root a file `image.qcow2` containing the image and a file `meta-data` in cloud-init meta-data
format that has a key `boot_disk` defining the device to write (only the short device name
like `sda`). If the key `debug_burner` is set to 1 in meta-data, the burner will just stop
at the end of the burning process, otherwise it reboots the server on the new disk.

* `-d` builds in debug mode with devuser with password
* `-k <keyfile>` adds a key for devuser
* `-o <name>` name of the burner (kernel and ramfs)

## ci-script.sh
The script that should be launched to create images. It relies on several environment variables describing
which images to build and how to annotate them on the Nexus.

* `REPO_URL`: the URL of the Nexus repository. Usually it should not be changed after the
  initial configuration.
* `IMAGE_VERSION`: the version number of all the OS images generated
* `K8S_VERSION`: a space separated list of k8s versions. k8s versions begin with a `v` eg `v1.18.8`
* `UBUNTU`: a space separated list of Ubuntu version names (only tested with `bionic`) 
  for which an image must be generated
* `CENTOS`: a space separated list of CentOS version names (only tested with `8`) 
  for which an image must be generated
* `UBUNTU_DEBUG`: a space separated list of Ubuntu version names (only tested with `bionic`) 
  for which a debug image must be generated. Debug images declare a user `devuser`
  with `secret` password that can be used to access the machine through the console
  when the network configuration is not working. The password
  can be redefined with the `SECRET` variable
* `CENTOS_DEBUG`: same as`UBUNTU_DEBUG` but for CentOS
* `STACK_VERSION`: the version of the stack (stack job)
* `RPMS`: **comma** separated list of RPM package to add to
   Centos images
* `DEBS`: **comma** separated list of debian package to add to
   Ubuntu images

